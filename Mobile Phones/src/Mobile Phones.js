function MobilePhone(model, touch, imageURL) {
    this.model = model;
    this.touch = touch;
    this.imageURL = imageURL;
}

angular.module("mobilesApp", []);
angular.module("mobilesApp").controller("mobileCtrl", ["$scope", function($scope) {
    $scope.mobiles = [
        new MobilePhone("1100", false, "./images/1100.jpeg"),
        new MobilePhone("S7 Edge", true, "./images/s7.jpeg"),
        new MobilePhone("iPhone 5S", true, "./images/5s.jpeg")
    ];
}]);