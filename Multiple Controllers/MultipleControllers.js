angular.module("booksApp", []);
angular.module("booksApp").run(function($rootScope) {
    $rootScope.onSubmit = function() {
        $rootScope.submitEnable = true;
    };
});
angular.module("booksApp").controller("techController", function($scope) {
    $scope.books = ["Fluid Dynamics", "Artificial intelligence", "Deep Learning"];

});
angular.module("booksApp").controller("nonTechController", function($scope) {
    $scope.books = ["Wings of Fire", "Stories", "Archeology"];
});