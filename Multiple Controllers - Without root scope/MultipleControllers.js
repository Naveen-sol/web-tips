var app = angular.module("booksApp", []);
angular.module("booksApp").controller("wrapperController", function($scope) {
    $scope.submitEnable = false;
    $scope.selectedNonTechBook = "Wings of Fire";
    $scope.selectedTechBook = "Fluid Dynamics";
    $scope.onSubmit = function() {
        $scope.submitEnable = true;
    };
});
angular.module("booksApp").controller("techController", function($scope) {
    $scope.books = ["Fluid Dynamics", "Artificial intelligence", "Deep Learning"];

});
angular.module("booksApp").controller("nonTechController", function($scope) {
    $scope.books = ["Wings of Fire", "Stories", "Archeology"];
});