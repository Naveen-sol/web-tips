# date-time-demo

This module is made just for getting a clear understanding on how npm works. Use this module at your own risk.

## How to Use?

node.js:

```
npm install date-time-demo
```

Using javascript: 

```javascript
var date-time-demo = require('date-time-demo');
```

You can also install this package globally and run this package directy from command line

```
datetimedemo
```