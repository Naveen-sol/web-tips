module.exports = {
    add: function (a, b) {
        return a + b;
    },
    sub: function (a, b) {
        return a - b;
    },
    divide: function (a, b) {
        return a / b;
    },
    mod: function (a, b) {
        return a % b;
    },
    multiply: function (a, b) {
        return a * b;
    }
}