var apiModule = require('./APIModule');
var arthemeticMod = require('./ArthemeticModule');

console.log(apiModule.isObjNull(null)); //true
console.log(apiModule.isObjNaN("sd")); //true
console.log(apiModule.isFunc("func")); //false
console.log(apiModule.isBool("true")); //false
console.log(apiModule.isObj({})); //true

console.log(arthemeticMod.add(1, 2)); //3
console.log(arthemeticMod.sub(4, 2)); //2
console.log(arthemeticMod.divide(4, 2)); //2
console.log(arthemeticMod.mod(10, 2)); //0
console.log(arthemeticMod.multiply(1, 2)); //2