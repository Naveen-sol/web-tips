function isNull(object) {
    if (object === null) {
        return true;
    }
    return false;
}

function isBoolean(object) {
    if (typeof object === 'boolean') {
        return true;
    }
    return false;
}

function isFunction(object) {
    if (typeof object === 'function') {
        return true;
    }
    return false;
}

function isObject(value) {
    if (value instanceof Object) {
        return true;
    }
    return false;
}

module.exports.isObjNull = isNull;
module.exports.isObjNaN = isNaN;
module.exports.isFunc = isFunction;
module.exports.isBool = isBoolean;
module.exports.isObj = isObject;